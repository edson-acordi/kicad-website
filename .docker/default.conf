# Expires map
map $sent_http_content_type $expires {
    default                    off;
    text/html                  epoch;
    ~image/                    2d;
    ~font/                     max;
    #css and js are fingerprinted by hugo pipes
    text/css                   max;
    application/javascript     max;
}

server {
    listen       8081;
    server_name www.kicad-pcb.org kicad-pcb.org;
    return 301 $scheme://www.kicad.org$request_uri;
}

server {
    listen       8081;
    server_name kicad.org;
    return 301 $scheme://www.kicad.org$request_uri;
}

server {
    listen       8081 default_server;
    server_name  _;
    server_tokens off;

    # avoid port changing between container and external ports on redirects
    # there is also port_in_redirect but it things trickier in development when you aren't binding to port 80 :/
    absolute_redirect off;

    #charset koi8-r;
    #access_log  /var/log/nginx/log/host.access.log  main;

    gzip on;
    gzip_disable        "MSIE [1-6]\.";
    gzip_vary           on;

    gzip_comp_level 4;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types
        # text/html is always compressed by HttpGzipModule
        text/css
        text/javascript
        text/xml
        text/plain
        text/x-component
        application/javascript
        application/json
        application/xml
        application/rss+xml
        font/truetype
        font/opentype
        application/vnd.ms-fontobject
        image/svg+xml;


    expires $expires;

    # Apply CSP policy to all requests
    # Fonts
    set $csp_font "font-src 'self'";
    # Allow google fonts
    set $csp_font "${csp_font} fonts.gstatic.com";
    # Allow our own domains
    set $csp_font "${csp_font} *.kicad.org";

    set $csp_style "style-src 'self'";
    # Allow the numerous style="" attributes scattered everywhere :/
    set $csp_style "${csp_style} 'unsafe-inline'";
    # Allow our own domains
    set $csp_style "${csp_style} *.kicad.org";
    # Allow google fonts
    set $csp_style "${csp_style} fonts.googleapis.com";

    set $csp_media "media-src 'self'";
    # Allow youtube embeds
    set $csp_media "${csp_media} *.youtube.com";
    set $csp_media "${csp_media} player.vimeo.com";
    # Allow our own domains
    set $csp_media "${csp_media} *.kicad.org";

    set $csp_object "object-src 'self'";
    # Allow youtube embeds
    set $csp_object "${csp_object} *.youtube.com";
    # Allow our own domains explicitly
    set $csp_object "${csp_object} *.kicad.org";

    set $csp_script "script-src 'self'";
    # Allow our webp inline script
    set $csp_script "${csp_script} 'unsafe-inline'";
    # Allow our own domains
    set $csp_script "${csp_script} *.kicad.org";
    # Cloudflare
    set $csp_script "${csp_script} static.cloudflareinsights.com";
    # Cloudflare
    set $csp_script "${csp_script} ajax.cloudflare.com";

    # Allow all frames that we set
    set $csp_frame "frame-src 'self'";
    # Allow our own domains
    set $csp_frame "${csp_frame} *.kicad.org";
    # Allow youtube embeds
    set $csp_frame "${csp_frame} *.youtube.com";
    # Allow osdn to trigger the autodownload, unforuantely they use multiple subdomains in a redirect
    set $csp_frame "${csp_frame} *.dl.osdn.jp";
    set $csp_frame "${csp_frame} osdn.net";
    set $csp_frame "${csp_frame} *.osdn.net *.rwth-aachen.de *.nchc.org.tw mirrors.gigenet.com mirrors.xtom.com";
    set $csp_frame "${csp_frame} mirrors.dotsrc.org mirrors.tuna.tsinghua.edu.cn mirrors.xtom.com.hk";
    set $csp_frame "${csp_frame} mirrors.bfsu.edu.cn mirror.liquidtelecom.com ftp.acc.umu.se osdn.mirror.constant.com";
    set $csp_frame "${csp_frame} mirror.math.princeton.edu plug-mirror.rcac.purdue.edu openbsd.c3sl.ufpr.br";
    set $csp_frame "${csp_frame} ftp.iij.ad.jp ftp.jaist.ac.jp ftp.onet.pl mirror.sjtu.edu.cn mirrors.nju.edu.cn";
    set $csp_frame "${csp_frame} player.vimeo.com";
    set $csp_frame "${csp_frame} mailto:";

    # Allow all external images
    set $csp_img "img-src *";
    # Allow youtube embeds
    set $csp_img "${csp_img} data:";
    
    set $csp_default "default-src 'self'";
    # Allow our own domains
    set $csp_default "${csp_default} *.kicad.org";

    add_header Content-Security-Policy "${csp_default};${csp_img};${csp_font};${csp_style};${csp_media};${csp_object};${csp_script};${csp_frame}";

    # Route for openshift/kubernetes/docker healthchecks
    location /healthz {
        access_log off;
        default_type text/plain;
        return 200 "healthy\n";
    }

    # Index
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
	
	###
	# New Website route changes
	###

    # Developer docs now get their own site
    location ~ ^/contribute/developers/(.*) {
        return 301 https://dev-docs.kicad.org/en/contribute;
    }

    # Docs were moved out to their own subdomain
    location ~ ^/help/documentation/(.*) {
        return 301 https://docs.kicad.org;
    }
    
    location ~ ^/help/getting-started/(.*) {
        return 301 https://docs.kicad.org;
    }

    location ~ ^/help/file-formats/(.*) {
        return 301 https://dev-docs.kicad.org/en/file-formats/;
    }
    
    # klc was split out
    location ~ ^/libraries/klc/(.*) {
        return 301 https://klc.kicad.org;
    }

	###
    # Legacy redirects
    # from when we used jira, ewww
	###
    location ~ ^/pages/viewpage.action/(.*) {
        return 301 https://docs.kicad.org;
    }
    
    location ~ ^/pages/viewrecentblogposts.action/(.*) {
        return 301 https://kicad.org/blog/;
    }

    location ~ ^/dashboard.action/(.*) {
        return 301 https://kicad.org/;
    }
    
    location ~ ^/plugins/servlet/mobile/(.*) {
        return 301 https://kicad.org/;
    }
    
    location ~ ^/display/DEV/(.*) {
        return 301 https://kicad.org/contribute/developers/;
    }
    
    location ~ ^/display/KICAD/Download(.*) {
        return 301 https://kicad.org/download/;
    }

    location ~ ^/display/KICAD/Windows(.*) {
        return 301 https://kicad.org/download/windows/;
    }
    
    location ~ ^/display/KICAD/Installing(.*) {
        return 301 https://kicad.org/download/;
    }

    location ~ ^/display/KICAD/Platforms(.*) {
        return 301 https://kicad.org/download/;
    }

    location ~ ^/display/KICAD/Tutorials(.*) {
        return 301 https://kicad.org/help/tutorials/;
    }

    location ~ ^/display/KICAD/Frequently+Asked+Questions(.*) {
        return 301 https://docs.kicad.org;
    }

    location ~ ^/display/KICAD/External+Tools(.*) {
        return 301 https://kicad.org/help/external-tools/;
    }

    location ~ ^/display/KICAD/KiCad+Documentation(.*) {
        return 301 https://docs.kicad.org;
    }
    
    location ~ ^/display/(.*) {
        return 301 https://kicad.org/;
    }

    # Typo in Tweet announcement
    location ~ ^/blog/2021/08/Aisler-Becomes-First-Platinum-Sponsor/(.*) {
        return 301 https://www.kicad.org/blog/2021/08/AISLER-Becomes-First-Platinum-Sponsor/;
    }

    # Reordered content /img/screenshots to /discover/screenshots
    location ~ ^/img/screenshots(?<name>/.*)?$ {
        root /usr/share/nginx/html;
        try_files /img/screenshots$name /discover/screenshots$name =404;
    }


    # The proxy is not currently working.  Needs debugging
    #location ~ ^/donate {
    #    proxy_pass http://kicad-donate.kicad-hosting.svc:5000;
    #}

    error_page 404 /404.html;
    location = /404.html {
        root   /usr/share/nginx/html;
        internal;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
