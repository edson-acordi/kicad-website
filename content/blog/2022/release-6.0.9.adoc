+++
title = "KiCad 6.0.9 Release"
date = "2022-10-31"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.9 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.8 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/20[KiCad 6.0.9
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.9 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/03ed7a4a270426a6a5d6856e15b4969fd8524030[Ensure the socket server is killed during destruction of the kiway player.]
- Save new library to current project path rather previously saved project path. https://gitlab.com/kicad/code/kicad/-/issues/12570[#12570]
- https://gitlab.com/kicad/code/kicad/-/commit/6ed3618ce5f86f96a86a2f332bcf530b1cf7b5ff[Improve numerical robustness of parallel and co-linear tests for segments of largely different lengths].
- Fix build failure with SWIG 4.1.0. https://gitlab.com/kicad/code/kicad/-/issues/12414[#12414]
- https://gitlab.com/kicad/code/kicad/-/commit/9f151025b3b5001ae1ad2b53b558c1d1d9f04ee7[Fix bug in BOX2 intersect method.]
- https://gitlab.com/kicad/code/kicad/-/commit/00ff5baf591534a5a60d779d377f6bf8e8f47958[Add quotes to restart Windows command line registration.]
- Add "Letter-like character" Omega for Unicode 0x2126. https://gitlab.com/kicad/code/kicad/-/issues/12531[#12531]
- https://gitlab.com/kicad/code/kicad/-/commit/4d729110d522d0f4938c32675217c95bf7c374e7[Fix C++17 build error.]

=== Schematic Editor
- Don't allow a save as operation to overwrite the root sheet. https://gitlab.com/kicad/code/kicad/-/issues/10872[#10872]
- Copy field properly. https://gitlab.com/kicad/code/kicad/-/issues/12376[#12376]
- Increase undo granularity of text editing inside text box. https://gitlab.com/kicad/code/kicad/-/issues/11756[#11756]
- Prevent printing hierarchical schematic on MacOS from being shifted down the page for each sheet. https://gitlab.com/kicad/code/kicad/-/issues/12211[#12211]
- Prevent netclass wire color and default sheet background color from switching to black. https://gitlab.com/kicad/code/kicad/-/issues/11963[#11963]
- Prevent black selection shadow and sheet backgrounds with "Default" language on Russian systems. https://gitlab.com/kicad/code/kicad/-/issues/12552[#12552]
- Fix brace highlighting when highlighted text is inverted. https://gitlab.com/kicad/code/kicad/-/issues/12467[#12467].
- Fix duplicate UUIDs when placing multiple sub-sheets. https://gitlab.com/kicad/code/kicad/-/issues/12588[#12588]
- Fix line width overflow crash. https://gitlab.com/kicad/code/kicad/-/issues/12555[#12555]
- Fix missing items when printing and/or plotting schematics. https://gitlab.com/kicad/code/kicad/-/issues/12559[#12559]
- Prevent BOM script "bom_csv_grouped_by_value_with_fp.py" from generating extra lines between each BOM line. https://gitlab.com/kicad/code/kicad/-/issues/10473[#10473]
- Show ERC errors when any label only has 1 pin. https://gitlab.com/kicad/code/kicad/-/issues/7203[#7203]
- Preload an empty circuit to avoid simulator error messages when no circuit is loaded. https://gitlab.com/kicad/code/kicad/-/issues/12481[#12481].
- https://gitlab.com/kicad/code/kicad/-/commit/8e331628b630ba1ceefaf4163acdcb85b62b1b99[Do not add a title/comment in mandatory field strings when writing netlists.]
- Avoid netlist export dialog crash from trying to delete a non-custom format. https://gitlab.com/kicad/code/kicad/-/issues/12229[#12229]
- Make electrical rules checker report conflicting power symbols on the same net. https://gitlab.com/kicad/code/kicad/-/issues/12138[#12138]

=== Symbol Editor
- Fix incorrect symbol display when reverting changes in derived symbols. https://gitlab.com/kicad/code/kicad/-/issues/10792[#10792]
- Fix crash when canceling from placing pin on top of another pin. https://gitlab.com/kicad/code/kicad/-/issues/12610[#12610]

=== CvPcb
- Maintain text filter box focus when search results change. https://gitlab.com/kicad/code/kicad/-/issues/12445[#12445]
- Attempt to reduce opportunity for lost keystrokes in search text box. https://gitlab.com/kicad/code/kicad/-/issues/12445[#12445]

=== PCB Editor
- Prune pasted data of non-enabled layers. https://gitlab.com/kicad/code/kicad/-/issues/11997[#11997]
- Make highlighted text readable in dark mode. https://gitlab.com/kicad/code/kicad/-/issues/10829[#10829]
- Allow vias to connect tracks with no net even when DRC checking is on. https://gitlab.com/kicad/code/kicad/-/issues/12403[#12403]
- Display cross-hairs in picker tools. https://gitlab.com/kicad/code/kicad/-/issues/10183[#10183]
- Fix crash when resetting grid origin or moving. https://gitlab.com/kicad/code/kicad/-/issues/12407[#12407]
- Fix select all across all groups. https://gitlab.com/kicad/code/kicad/-/issues/12411[#12411]
- Fix via net inheritance when working on other layers. https://gitlab.com/kicad/code/kicad/-/issues/11487[#11487]
- Fix layer expression parsing bugs in custom DRC processor. https://gitlab.com/kicad/code/kicad/-/issues/12437[#12437]
- Allow free pad usage in router. https://gitlab.com/kicad/code/kicad/-/issues/11730[#11730]
- Do not show disambiguation menu on track corners. https://gitlab.com/kicad/code/kicad/-/issues/10745[#10745]
- Fix broken preset view layer selection drop down when Chinese language selected. https://gitlab.com/kicad/code/kicad/-/issues/12227[#12227]
- Fix crash on cut command (Ctrl+X). https://gitlab.com/kicad/code/kicad/-/issues/12562[#12562]
- https://gitlab.com/kicad/code/kicad/-/commit/e834374ad261e5265ac53aac4b54e41a777d558b[Use hole plating size for visibility only, not zone filling, routing or DRC.]
- Make graphic line properties dialog OK button work correctly in some locales. https://gitlab.com/kicad/code/kicad/-/issues/12176[#12176]
- Fix incorrect behavior of preset layers widget in non English languages. https://gitlab.com/kicad/code/kicad/-/issues/12227[#12227]
- Fix cut command (Ctrl+X) crash. https://gitlab.com/kicad/code/kicad/-/issues/12562[#12562]
- https://gitlab.com/kicad/code/kicad/-/commit/e834374ad261e5265ac53aac4b54e41a777d558b[Use hole plating size for visibility only, not zone filling, routing or DRC.]
- Fix crash on Specctra import. https://gitlab.com/kicad/code/kicad/-/issues/12583[#12583]
- Fix crash when dragging a via. https://gitlab.com/kicad/code/kicad/-/issues/12612[#12612]
- https://gitlab.com/kicad/code/kicad/-/commit/292492bd011f4e6055e9e2722acb0ad1041914d8[Prevent crashes when canceling duplication.]
- Fix uncaught minimum annular ring on through-hole pads design rule violation. https://gitlab.com/kicad/code/kicad/-/issues/12109[#12109]
- Fix out of range differential pair gap that is reported as minimum value. https://gitlab.com/kicad/code/kicad/-/issues/12587[#12587]
- Show correct net in message bar after changing layers when routing differential pair. https://gitlab.com/kicad/code/kicad/-/issues/12592[#12592]
- Fix custom rules insideArea defined as F.Cu only from propagating to other layers. https://gitlab.com/kicad/code/kicad/-/issues/12584[#12584]
- Fix crash when setting size of rectangular pad to 0 x 0. https://gitlab.com/kicad/code/kicad/-/issues/12605[#12605]
- https://gitlab.com/kicad/code/kicad/-/commit/2793e67bab8e403fd1f5c21c82190aa5c44eca94[Bring adding to group and hiding in sync.]
- Prevent footprint from temporarily disappearing when selection clarification menu is opened. https://gitlab.com/kicad/code/kicad/-/issues/12547[#12547]
- Fix crash if multiple items are selected and drag command is issued. https://gitlab.com/kicad/code/kicad/-/issues/12460[#12460]
- Prevent differential pair router placing tracks overlapping the starting via with "Connected layers" annular rings setting. https://gitlab.com/kicad/code/kicad/-/issues/12458[#12458]
- Force DRC rules to be re-evaluated when switching layers in routing tool. https://gitlab.com/kicad/code/kicad/-/issues/12576[#12576]
- Fix missing footprint complex pads. https://gitlab.com/kicad/code/kicad/-/issues/12617[#12617]
- Import board stack up when importing setting from another board. https://gitlab.com/kicad/code/kicad/-/issues/10925[#10925]
- Fix crash when deleting track with "Delete Clicked Items". https://gitlab.com/kicad/code/kicad/-/issues/12600[#12600]
- Fix crash when adding new via. https://gitlab.com/kicad/code/kicad/-/issues/12604[#12604]
- Fix incorrect behavior when adding a new dielectric layer to stack up manager. https://gitlab.com/kicad/code/kicad/-/issues/12680[#12680]
- Re-evaluated DRC rules when switching layers in routing tool. https://gitlab.com/kicad/code/kicad/-/issues/12576[#12576]
- Fix message bar reporting "(no net)" after changing layers when routing differential pair. https://gitlab.com/kicad/code/kicad/-/issues/12592[#12592]
- Don't connect disparate nets. https://gitlab.com/kicad/code/kicad/-/issues/12622[#12622]
- Fix selection and entering in nested groups. https://gitlab.com/kicad/code/kicad/-/issues/12586[#12586]
- https://gitlab.com/kicad/code/kicad/-/commit/152252c6858e849b08f154f24dd2aa2865b22ad4[Fix selecting pads of footprints in groups.]
- https://gitlab.com/kicad/code/kicad/-/commit/909358e643ea15031529ea6f044827d57a8f6dc2[Ensure differential pair caches are layer-specific.]
- Fix broken custom clearance rules for vias. https://gitlab.com/kicad/code/kicad/-/issues/12733[#12733]
- Improve logic of position relative tool. https://gitlab.com/kicad/code/kicad/-/issues/12672[#12672]
- Check clearance on all layers of multi-layer items. https://gitlab.com/kicad/code/kicad/-/issues/12733[#12733]
- Improve logic of "Position Relative" tool. https://gitlab.com/kicad/code/kicad/-/issues/12672[#12672]
- Fix crash when trying to route differential pair. https://gitlab.com/kicad/code/kicad/-/issues/12747[#12747]

=== Footprint Editor
- Fix missing solder paste layer in 3D view panel in the footprint properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/12631[#12631]
- Fix crash when moving line then pressing escape quickly. https://gitlab.com/kicad/code/kicad/-/issues/12553[#12553]
- Prevent turning all elements of the pad into graphic shapes when changing user grid exits custom pad edit mode. https://gitlab.com/kicad/code/kicad/-/issues/12630[#12630]
- Fix "connected layers" pad property not copied to other pads. https://gitlab.com/kicad/code/kicad/-/issues/12691[#12691]
- https://gitlab.com/kicad/code/kicad/-/commit/6de30b19a0d9e0b4af5aa7601743fc917c2e9b62[Don't sync the world twice in inline router tools.]
- Fix crash when selecting a complex board with low GPU memory. https://gitlab.com/kicad/code/kicad/-/issues/12117[#12117]

=== 3D Viewer
- Fix 3D preview colors issue when in Serbian locale. https://gitlab.com/kicad/code/kicad/-/issues/12002[#12002]
- https://gitlab.com/kicad/code/kicad/-/commit/0593663462bdf4af48830a7afbfbe9c9ca2c3b01[Fix canvas timer event clash.]

=== Gerber Viewer
- Fix disappearing Gerber elements. https://gitlab.com/kicad/code/kicad/-/issues/12636[#12636]
- Make Linux mime-type associations recognize Mentor-generated gerber files. https://gitlab.com/kicad/code/kicad/-/issues/12097[#12097]

=== Page Layout Editor
- Fix crash when trying to delete an item. https://gitlab.com/kicad/code/kicad/-/issues/12717[#12717]

=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/594beef22e5e9eb986feba1f6a35bde0bf917426[Bump vcpkg builds to wxWidgets to 3.2.1.]
