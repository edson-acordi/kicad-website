+++
title = "KiCad 5.1.12 Release"
date = "2021-11-10"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 5 stable
release.  The 5.1.12 stable version contains critical bug fixes and
other minor improvements since the previous release.  If you are
wondering why there was no official 5.1.11 stable release, it's
because there was an issue with the 5.1.11 tagged code which some
users downloaded before the actual release announcement.  This
required a fix and a new release tag.  The KiCad project would like
to apologize for any inconvenience this may have caused.

<!--more-->

A list of all of the fixed bugs since the 5.1.10 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/6[KiCad 5.1.11
milestone page].  This release contains several critical bug fixes so
please consider upgrading as soon as possible.

Version 5.1.12 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/5.1/[5.1] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

NOTE: KiCad no longer supports macOS older than Mojave 10.14 as of the
5.1.7 release.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.
